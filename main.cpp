#include <LightGBM/c_api.h>

#include <iostream>
#include <cstdio>
#include <random>
#include <algorithm>

int main(int argc, char** argv)
{	
	int res;
	int booster_it;
	BoosterHandle handle;

	res = LGBM_BoosterCreateFromModelfile("test_model.txt", &booster_it, &handle);
	std::cout << "Create from model file result: " << res << std::endl;

	res = LGBM_BoosterPredictForFile(handle, "iris.test", 0, C_API_PREDICT_NORMAL, 0, 0, "", "out.txt");
	std::cout << "Predict for file result: " << res << std::endl;

	std::vector<float> row={5.3, 2.9, 1.6, 0.2};
	void* in_p = static_cast<void*>(row.data());

	std::vector<double> out(3, 0);
	double* out_result = static_cast<double*>(out.data());

	int64_t out_len;
	res = LGBM_BoosterPredictForMat(handle, in_p, C_API_DTYPE_FLOAT32, 1, 4, 1, C_API_PREDICT_NORMAL, 0, 2, "", &out_len, out_result);
	std::cout << "Predict res: " << res << std::endl;
	std::cout << "Prediction: (" << out_result[0] << ", " << out_result[1] << ", " << out_result[2] << ")" << std::endl;

	return 0;
}
